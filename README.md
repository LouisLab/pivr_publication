# PiVR_publication

Contains all the scripts and will also contain all the data of the
 PiVR publication.

**Note: We used a Windows computer while writting all the scripts. We 
tried to make it compatible with Linux based systems, but haven't 
tested all scripts! Let us know if you run into any problem: 
mlouis_at_ucsb_dot_edu**

**Note: In case you are thinking of just using these scripts to
analyze data you collected with your own PiVR setup: Please be aware
that some of the data in this repository has been collected with
early versions of the PiVR software using different names for
different files. You will have to adapt these in the scripts to
analyze data collected with current version of PiVR.**

# Installation necessary packages

1. Install either Miniconda (https://conda.io/miniconda.html) or 
Anaconda (https://www.anaconda.com/download/).
2. Install Git (https://git-scm.com/downloads) (already installed on 
many Linux distributions!)

3. Open Terminal(Mac/Linux) or Command Prompt(Windows)

4. Create a new environment using conda:
    
    `conda create -n pivr_publication python=3.6 -y`

5. Activate the environment in Windows by typing:

    ``activate pivr_publication``
    
    on Mac/Linux type:
    
    ```source activate pivr_publication```

6. Install necessary packages by typing all of the below:

    `conda install numpy -y`
    
    `conda install jupyter -y`
    
    `conda install matplotlib -y`
    
    `conda install pandas -y`
    
    `conda install -c anaconda pillow -y`
    
    `conda install scipy -y`
    
    `conda install -c conda-forge scikit-image -y`
    
    `conda install -c conda-forge ffmpeg -y`
    
    `conda install natsort -y`
    
    `pip install scikit-posthocs`

    `pip install imageio-ffmpeg`

    `pip install ffmpeg-python`
       
7. Change directory to where you've downloaded the data package

8. Type:
    
    `jupyter notebook`
    
9. Your standard browser should open (i.e. Chrome) and one tab should
   say "Home". This is the jupyter notebook. Most scripts in this
   repository are jupyter notebooks.
   
10. Some of the scripts are pure python scripts. To run these, just
    cd to the relevant folder (e.g. "Movies") and run the script by
    typing "python Movie_1.py".
    