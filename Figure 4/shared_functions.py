import numpy as np
from pathlib import Path
from scipy.ndimage.interpolation import affine_transform

class Constants():
    """
    In order not to have to redefine the constants constantly (Pun
    not intended) prepare a class that contains them.
    This should make it straightforward to change them and apply the
    identical constants all over the analysis scripts
    """

    def __init__(self):

        # how many frames before and after the peak of the scoot are
        # taken into consideration when calculating the angle the
        # fish is moving? 9 Seems a good starting point: 18*33ms =
        # ~600ms, about half the time a scoot!
        self.scoot_time = 9

        # maximum allowed turn angle to count as a scoot:
        # Rationale: The control fish really love to hang out at the
        # edge of the petri dish. The tracker often mistakes the
        # mirror image of the fish as the fish. This leads to a lot
        # of 'jumping' of the centroid which in turns leads to a lot
        # of opposing angles (e.g. 180degree, -180degree etc.). We
        # only count scoots/turns if the angle is below the
        # max_turn_angle!
        self.max_turn_angle = 3 * np.pi / 4

        # Some constants needed to define scoots
        self.speed_scoot = 2.5  # in mm/s
        self.min_frames_between_scoots = 5  # frames, one frame =0.033s
        self.minimum_length_of_scoot = 5  # in frames
        # take_data_up_to = 3*60*30
        self.max_distance_to_source = 40 # in mm

        # create the kernel used to filter x/y coordinates before
        # calculating speed and distance to source
        # open triangle kernel - starts at maximum in center, ignores
        # first half (-1), then linear slope
        kernel_length = 30  # needs to be odd!!
        # create the series
        kernel = np.arange(kernel_length)
        # invert all values
        kernel = kernel_length - kernel
        # cut of in center
        kernel[0:int(np.ceil(kernel.shape[0] / 2))] = 0
        # normalize to one
        self.kernel_centroid = kernel / np.sum(kernel)

        # full triangle filtering of the speed
        # create the series
        kernel = np.asarray([1, 2, 3, 4, 5, 4, 3, 2, 1])
        # normalize to one
        self.kernel_speed = kernel / np.sum(kernel)

constants = Constants()

# make sure all angles are going from 0 to 360 degrees!
def transpose_angle_to_2pi(list_of_lists):
    """
    """
    output = []
    for i_outer in range(len(list_of_lists)):
        temp = []
        for i_inner in range(len(list_of_lists[i_outer])):
            temp.append(
                (list_of_lists[i_outer][i_inner]
                 + 2 * np.pi)
                % (2 * np.pi))

        output.append(temp)

    return (output)

def calc_angle_diff(a,b):
    """
    use this function to calculate the shortest distance between two
    angles.
    This function solves  problem that it's not a simple subtraction
    to calculate the distance between two angles as there is a
    'break', i.e. since angles are defined in a circle the angles
    0.1 and 2*pi - 0.1 are only 0.2 radians away, not 2*pi-0.2!
    IMPORTANT: Use with values going from 0 - 2*pi!
    """
    if a - b > np.pi:
        result = 2*np.pi - a + b
    elif a - b < -np.pi:
        result = 2*np.pi + a - b
    else:
        result = a - b
    return(result)

def calc_angle_diff_2(a, b):
    """
    another calc_angle function-Also takes values between 0..2pi.
    This is different from the other version as it keeps the
    orientation relative to the origin encoded in the sign of the
    output. If animal is turning to the left the sign is positive,
    if it's turning to the right it's negative.
    In principle this function will take vector a and put it to angle
    zero (pointing to the right). In order to properly align vector b
    it needs to either subtract or, if crossing the 2pi/0 boundary,
    subtract
    or add 2pi
    """
    if a - b > np.pi:
        output = (a - b) - 2 * np.pi
    elif a - b < -np.pi:
        output = 2 * np.pi + (a - b)
    else:
        output = a - b
    return (output)


def define_counted_scoots(identified_peaks,
                          scoot_time,
                          centroid_coords,
                          y_source,
                          x_source
                          ):
    """
    Make a function to define counted scoots - reason: Make sure I'm
    analyzing both datasets exactly the same way! Also easier to plug
    in more criteria or change them.

    Idea:
    During each scoot, the animal can either turn more or less
    towards the source.
    In the shared_functions.defined_counted_scoots() the following
    angles are calculated:

    animal_angle: Used coordinates 9 frames before and after peak
    speed to identify travelled direction. The 9 frames should be
    enough for the fry to finish the scoot, these coordinates should
    therefore indicate the stops between the scoot.

    angle_front_source: Calculate angle between coordinates 9 frames
    after the peak speed and the source.

    Both of those angles are then converted from -pi..+pi to 0..+2pi
    and the difference in the angle calculated
    """

    angles_rel_to_light = []
    counted_scoots = []
    all_angle_temp = []
    not_counted_scoots = []

    for i_scoot_angle in range(identified_peaks.shape[0]):

        y_before = centroid_coords[
            identified_peaks[i_scoot_angle] - scoot_time, 0]

        y_after = centroid_coords[
            identified_peaks[i_scoot_angle] + scoot_time, 0]

        x_before = centroid_coords[
            identified_peaks[i_scoot_angle] - scoot_time, 1]

        x_after = centroid_coords[
            identified_peaks[i_scoot_angle] + scoot_time, 1]

        if not y_before == y_after and not x_before == x_after:

            # only calculate an angle if there is a difference in the
            # position of the animal 100ms before and 100ms after the
            # maxium of the scoot!

            # temporarily calculate the angle of the animal in the
            # absolute coordinate system
            temp = np.arctan2(y_after - y_before, x_after - x_before)

            try:
                # calculate the angle between the current and last
                # animal angle
                temp_animal_angle = calc_angle_diff_2(temp,
                                                      angle_animal)

                # max_turn_angle goes here - see reason above where
                # this variable is defined!
                # if change of animal in direction is larger than
                # defined by max_turn_angle, don't count the scoot!
                if -constants.max_turn_angle \
                        < temp_animal_angle \
                        < constants.max_turn_angle:

                    # Calculate the current theta by calculating the
                    # angle between the coordinates 9 frames (0.6s)
                    # before and 9f frames after the peak speed (the
                    # scoot!)
                    angle_animal = np.arctan2(y_after - y_before,
                                              x_after - x_before)

                    # Calculate the angle between the source and the
                    # position 9 frames after the peak speed (the
                    # scoot).
                    angle_front_source = np.arctan2(y_source - y_after,
                                                    x_source - x_after)

                    # convert angles from -pi..pi to 0..2pi
                    angle_animal_corr = (angle_animal + 2 * np.pi) \
                                        % (2 * np.pi)
                    angle_front_source_corr = \
                        (angle_front_source + 2 * np.pi) \
                        % (2 * np.pi)
                    # Calculates the difference in angle between the
                    # animal movement and the angle between animal
                    # and the source.
                    angles_rel_to_light.append(
                        calc_angle_diff_2(angle_front_source_corr,
                                          angle_animal_corr))

                    counted_scoots.append(
                        identified_peaks[i_scoot_angle])

                    all_angle_temp.append(angle_animal)
                else:
                    not_counted_scoots.append(identified_peaks[i_scoot_angle])
            except UnboundLocalError:
                angle_animal = np.arctan2(y_after - y_before,
                                          x_after - x_before)
                angle_front_source = np.arctan2(y_source - y_after,
                                                x_source - x_after)
                angle_animal_corr = \
                    (angle_animal + 2 * np.pi) % (2 * np.pi)
                angle_front_source_corr = \
                    (angle_front_source + 2 * np.pi) % (2 * np.pi)

    return (counted_scoots, angles_rel_to_light, angles_rel_to_light,
            all_angle_temp,not_counted_scoots)

def adjust_VR_arena_to_animal(data_first_frame,
                              centroid_coordinates,
                              home_path):
    """
    In the control case, no arena was used.

    To be able to compare this to the experimental condition,
    the same code that was used to prepare the arena in the
    experimental condition is applied post-hoc using this function.
    """

    # take the standard gaussian gradient that was used by all the
    # gaussian experiments
    arena_name = '640x480_gaussian_centred_animal_pos[250,240,0.0].csv'
    standard_arena = np.genfromtxt(Path(home_path, 'data/', arena_name),
                                   delimiter=',')  # work laptop

    # some generally true variables as all experiments here employ
    # the same arena
    placed_animal_x = int(
        arena_name.split('animal_pos')[1].split('[')[1].split(',')[0])
    placed_animal_y = int(arena_name.split(',')[1].split('[')[0])
    placed_animal_theta = float(
        arena_name.split('animal_pos')[1].split(']')[0].split(',')[-1])
    # frame_number = 0

    ###########
    # Translation/Rotation depending on the animal
    # take the standard gaussian arena that is still centered to the
    # middle of the image and do the translation/rotation
    # that would happen during the experiment.

    # Difference of the real animal coordinates and the desired
    # coordinates as defined in the VR arena
    diff_x = data_first_frame[0]['centroid col'] - placed_animal_x
    diff_y = data_first_frame[0]['centroid row'] - placed_animal_y

    # First the difference of the real and the desired angle needs to
    # be calculated:

    # The coordinates of the first original animal in int32 space (
    # originally delivered as int16) row/Y THEN # column/X
    first_animal_coordinates = [
        np.asarray(int(data_first_frame[0]['centroid row'])).astype(
            np.int32),
        np.asarray(int(data_first_frame[0]['centroid col'])).astype(
            np.int32)]

    # The coordinates of the animal after it left the original
    # bounding box. As the original coordinate is not saved the first
    # centroid that is detected during the experiment is taken.
    # row/Y THEN # column/X
    before_exp_start_animal_coordinates = \
        [(centroid_coordinates[0, 0]).astype(np.int32),
         (centroid_coordinates[0, 1]).astype(np.int32)]
    # These two coordinates are used to calculate the direction angle
    # that the animal is coming from. The origin is the first animal.
    # row/Y THEN # column/X
    real_animal_angle = np.arctan2(
        before_exp_start_animal_coordinates[0] -
        first_animal_coordinates[0],
        before_exp_start_animal_coordinates[1] -
        first_animal_coordinates[1])
    # Then the difference in the real angle of the experiment and the
    # desired angle defined by the VR Arena is calculated
    difference_angle = placed_animal_theta - real_animal_angle

    ###############################
    # In for historical reasons:
    # I've run experiments until September 12th, 2018 with this
    # setting. The problem is that whenever the angle was negative
    # the orientation was inverted! Due to that I was only able to use ~
    # 50% of the collected data so far.
    ##############################
    # Next, all the angles that are currently going from [-0..-pi] (
    # due to arctan2) to are being converted to [pi..2*pi]
    # difference_angle = (difference_angle + np.pi) % np.pi

    # Goal here is to be explicit as speed is probably not really of
    # the essence as this only happens once.

    # First the defined (= placed_animal) coordinates are set to
    # become the origin
    rotation_origin = np.array(
        [[1, 0, -placed_animal_y],  # y-coordinate
         [0, 1, -placed_animal_x],  # x-coordinate
         [0, 0, 1.]])
    # Then the whole arena is rotated at the origin by the difference
    # in angle between the real animal and the defined angle (
    # =placed_animal)
    rotation = np.array(
        [[np.cos(difference_angle), -np.sin(difference_angle), 0],
         [np.sin(difference_angle), np.cos(difference_angle), 0],
         [0, 0, 1]])
    # Then the position of the animal is set back to the original position
    rotation_back = np.array([[1, 0, placed_animal_y],  # y-coordinate
                              [0, 1, placed_animal_x],  # x-coordinate
                              [0, 0, 1.]])
    # Finally the difference between the x and y position of the real
    # animal and the desired position
    # is stated
    animal_position_difference = np.array([
        [1, 0, diff_y],
        [0, 1, diff_x],
        [0, 0, 1]
    ])
    # Now all these matrices are multplied (@ operator does same as numpy.dot)
    transform = animal_position_difference @ rotation_back @ rotation @ rotation_origin
    '''
    ----   --                 --   --               --   --          --   --                --
    |y'|   |1, 0, Y difference |   |1, 0, Y VR animal|   |cos, -sin, 0|   |1, 0, -Y VR animal| 
    |x'| = |0, 1, X difference | x |0, 1, X VR animal| x |sin, cos,  0| x |0, 1, -X VR animal|
    |0 |   |0, 0, 1            |   |0, 0, 0          |   |0,   0,    1|   |0, 0, 0           |
    ----   --                 --   --               --   --          --   --                --
    '''
    # Need to inverse the transformation matrix (peculiarity of how
    # affine_transform is implemented in scipy.ndimage)
    transformation_matrix = np.linalg.inv(transform)

    # Do an affine tranformation of the original arena using the
    # scipy.ndimage.affine_transform function
    # https://docs.scipy.org/doc/scipy-0.19.1/reference/generated/scipy.ndimage.affine_transform.html
    arena_rotated_translated = affine_transform(
        standard_arena,
        transformation_matrix[:2, :2],
        offset=transformation_matrix[:2, 2],
        cval=0.0,
        order=1)

    return (arena_rotated_translated)