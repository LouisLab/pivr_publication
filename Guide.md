# List of scripts to use to create each plot in the paper

Use this list to find the python scripts used to create each
plot from the paper. 

It is assumed that you have unzipped each of the 'Figure x.zip' files
in the identically named folder. E.g. 'Figure 1.zip' has been unzipped
in 'Figure 1' folder.

Files that end with 'ipynb' need to be opened with jupyter. See the
'README.md' file for instrctions.

## Main Figures:

Fig1c:	 'Figure 1/Fig1c_VR_explanation.ipynb'
###

Fig2a: 	 'Figure 2/Fig2a_larva_defined.ipynb'

Fig2b:	 'Figure 2/Fig2bc_speed_plot.ipynb'

Fig2c:	 'Figure 2/Fig2bc_speed_plot.ipynb'

Fig2di	 'Figure 2/Fig2di_ei_fi_beautiful_tracks.ipynb'

Fig2ei	 'Figure 2/Fig2di_ei_fi_beautiful_tracks.ipynb'

Fig2fi	 'Figure 2/Fig2di_ei_fi_beautiful_tracks.ipynb'

Fig2dii	 'Figure 2/Fig2dii_eii_fii_densityPlotsLarvae.ipynb'

Fig2eii  'Figure 2/Fig2dii_eii_fii_densityPlotsLarvae.ipynb'

Fig2fii  'Figure 2/Fig2dii_eii_fii_densityPlotsLarvae.ipynb'

Fig2diii 'Figure 2/Fig2dii_eii_fii_densityPlotsLarvae.ipynb'

Fig2eiii 'Figure 2/Fig2dii_eii_fii_densityPlotsLarvae.ipynb'

Fig2fiii 'Figure 2/Fig2dii_eii_fii_densityPlotsLarvae.ipynb'
###

Fig3a:	 'Figure 3/Fig3a_fly_defined.ipynb'

Fig3b:	 'Figure 3/Fig3b_example_trajectory.ipynb'

Fig3c:	 'Figure 3/Fig3c_preference_index.ipynb'

Fig3d:	 'Figure 3/Fig3d_Ethogram.ipynb'

Fig3e:	 'Figure 3/Fig3e_f_speed_plots.ipynb'

Fig3f:	 'Figure 3/Fig3e_f_speed_plots.ipynb'
###

Fig4a:	 'Figure 4/Fig4a_animal_defined.ipynb'

Fig4b:	 'Figure 4/Fig4b_c_exp_traj_speed.ipynb'

Fig4c:	 'Figure 4/Fig4b_c_exp_traj_speed.ipynb'

Fig4d:	 'Figure 4/Fig4d_all_trajectories.ipynb'

Fig4e:	 'Figure 4/Fig4e_Fig8Sa_distance_to_source.ipynb'

Fig4g:	 'Figure 4/Fig4g_h_j_TurnAngle_vs_Intensity_and_TurnIndex_vs_Intensity.ipynb'

Fig4h:	 'Figure 4/Fig4g_h_j_TurnAngle_vs_Intensity_and_TurnIndex_vs_Intensity.ipynb'

Fig4j:	 'Figure 4/Fig4g_h_j_TurnAngle_vs_Intensity_and_TurnIndex_vs_Intensity.ipynb'


## Supplementary Figures

S1Fig b:	'Figure 1/Fig1S1_b.ipynb'

S1Fig c:	'Figure 1/Fig1S1_c.ipynb'

S1Fig d:	'Figure 1/Fig1S1_d.ipynb'

S1Fig f:	'Figure 1/Fig1S1_F.ipynb'
###

S2Fig ci:	'Figure 1/Fig1S2ci_low_power_red_LED_measurement.ipynb'

S2Fig di:	'Figure 1/Fig1S2di_high_power_led_measurement.ipynb'
###

S3Fig: a-n:	'Figure 1/Fig1S3_DetectionModes.ipynb'
###

S4Fig: a-h:	'Figure 1/Fig1S4_Tracking_Flowchart.ipynb'
###

S5Fig: a-d:	'Figure 1/Fig1S5_head_tail_classification.ipynb'
###

S6Fig: a-d: 'Figure 1/Fig1S6_sample_trajectories_other_animals.ipynb' &
			'Figure 1/Fig1S6a_KelpFly_defined.ipynb' &
			'Figure 1/Fig1S6b_spider_defined.ipynb' &
			'Figure 1/Fig1S6c_firefly_defined.ipynb' &
			'Figure 1/Fig1S6c_woodlouse_defined.ipynb'
###			
			
S7Fig a-c:	'Figure 2/Fig2diii_eiii_fiii_distance_to_source_plot.ipynb'
###

S8Fig a:	'Figure 4/Fig4e_Sup1a_distance_to_source.ipynb'

S8Fig b:	'Figure 4/Fig4Sup1b_TurnAngle_vs_OrientationToLight.ipynb'
			
# Other supplementary information

*Note: Files ending with 'py' can be run by typing 'python Movie_1.py'
directly in the console when the pivr_publication environment has been
activated.*

S1 Movie:	'Movies/Movie_1.py'

S2 Movie:	'Movies/Movie_2.py'

S3 Movie:	'Movies/Movie_3.py'

S4 Movie:	'Movies/Movie_4.py'

S5 Movie:	'Movies/Movie_5.py'

S6 Movie:	'Movies/Movie_6.py'