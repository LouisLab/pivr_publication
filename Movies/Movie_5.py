"""
D. melanogaster adult with genotype Gr66a>Chrimson in a checkerboard
virtual reality!
"""

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from matplotlib import animation
import matplotlib.gridspec as gridspec
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pathlib import Path
import json
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
import matplotlib.font_manager as fm
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.patches as mpatches

fps = 30
video_name = 'Movie_5_new.mp4'
# How long in the past should the head and centroid position be plotted?
past_trajectory = 20  # in seconds

# FOR TESTING
no_of_frames = 5400  # Set to False to get full video! (
# Well, almost full, the last 5 frames (of 9000) are not taken into
# account) - only do first 3 minutes as nothing intersesting is
# happening afterwards!

# colormap = 'magma'
# viridis
# plasma
# inferno
# magma

# Organize the path - everything's relative to the location of the
# script being used!
home_path = os.getcwd()
target_path = Path(home_path + '/Created_Movies/')
os.makedirs(target_path, exist_ok=True)
data_path = Path(home_path +
                 '/data/04.10.2018_16-49-15_GR66a-Gal4xUAS'
                 '-Chrimson/')
os.chdir(data_path)

# Read the binary images...
raw_images = np.load('sm_thresh.npy')  # todo rename!
# ...and the coordinates the each image is located...
bounding_boxes = np.load('bounding_boxes.npy')
# ...and the delivered stimulation
stimulation = np.load('stimulation.npy')
# and finally the stimulation file for the background
stimulation_file_original = \
    np.genfromtxt(
        '640x480_checkerboard_10%.csv',
        delimiter=',')
# Need to read the data.csv file as well
path_object = Path(data_path).glob('**/*')
files = [x for x in path_object if x.is_file()]
for i in files:
    if 'data.csv' in str(i):
        data_csv = pd.read_csv(i, delimiter=',')

with open(('experiment_settings.json'), 'r') as file:
    experiment_settings = json.load(file)
    pixel_per_mm = experiment_settings['Pixel per mm']
    recording_time = experiment_settings['Recording time']
    recording_fps = experiment_settings['Framerate']

past_trajectory *= recording_fps

# create first image, make it a 3 channel image
#stimulation_file = np.zeros((stimulation_file_original.shape[0],
#                             stimulation_file_original.shape[1],
#                             3))

# get the virtual arena in uW/mm2
measured_light_intensity = 4.5

# IMPORTANT: For the high powered version the values are inverted!
# 40000 is OFF and 0 is 100% ON
# Do it by hand as we only have two values anyway!
stimulation[stimulation < 40000] = measured_light_intensity
stimulation[stimulation == 40000] = 0
stim_in_uW_mm = stimulation

stimulation_file_original[stimulation_file_original < 40000] = measured_light_intensity
stimulation_file_original[stimulation_file_original == 40000] = 0

cbar_bool = []


def own_red_colormap():
    """
    Originally I modified the 'Reds' colormap. The problem was that at value 0 it was still a bit red and
    not white.
    I'm using the starting values Reds for the green and blue channel.
    The red channels stays always on one.
    Finally, the inverted(!) array is passed to LinearSegmentedColormap to create the cmap object
    to be used for plotting
    """
    own_colors = np.zeros((120, 4))
    own_colors[:, 0] = 1  # red channel
    own_colors[:, 1] = np.linspace(0.41279508, 1,
                                   own_colors.shape[0])  # green channel
    own_colors[:, 2] = np.linspace(0.28835063, 1,
                                   own_colors.shape[0])  # blue channel
    own_colors[:, 3] = 1

    own_colormap = LinearSegmentedColormap.from_list('Lower Half',
                                                     own_colors[::-1,
                                                     :])

    return (own_colormap)

own_red_cmap = own_red_colormap()

def prepare_array(i):
    """
    Prepare the array for plotting
    """
    stimulation_file = np.zeros((stimulation_file_original.shape[0],
                                 stimulation_file_original.shape[1],
                                 3))

    # Valid floating point input is 0..1 - normalize
    stimulation_file[:, :, 0] = stimulation_file_original.copy()

    row_min = bounding_boxes[0, i]
    row_max = bounding_boxes[1, i]
    col_min = bounding_boxes[2, i]
    col_max = bounding_boxes[3, i]

    area_of_animal = stimulation_file[row_min:row_max, col_min:col_max,
                     :]

    # turn the blue channel ON
    area_of_animal[
        raw_images[0:int(bounding_boxes[1, i] - bounding_boxes[0, i]),
        0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
        i], 2] = 1

    # and the others off to only have blue left
    area_of_animal[
        raw_images[0:int(bounding_boxes[1, i] - bounding_boxes[0, i]),
        0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
        i], 0] = 1

    area_of_animal[
        raw_images[0:int(bounding_boxes[1, i] - bounding_boxes[0, i]),
        0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
        i], 1] = 1

    # max value in stim file is 40000
    arena_in_uW_mm = stimulation_file[:, :, 0] # leftover from the
    # other scripts!
    return (arena_in_uW_mm, stimulation_file)


def updatefig(i):
    print(i)

    ax_overview.cla()

    arena_in_uW_mm, stimulation_file = prepare_array(i)

    # use masked arrays to mask the image background where the animal
    # is and mask the parts where the animal is not on the animal axis
    animal_masked = np.ma.masked_array(
        stimulation_file[:, :, 2])  # ,stimulation_file[:,:,2]>0)
    arena_masked = np.ma.masked_array(arena_in_uW_mm,
                                      stimulation_file[:, :, 2] > 0)

    plot_animal = ax_overview.imshow(animal_masked,
                                     animated=True,
                                     interpolation='nearest',
                                     cmap='Blues')

    plot_background = ax_overview.imshow(
        arena_masked,
        animated=True,
        interpolation='nearest',
        cmap=own_red_cmap,
        vmin=np.amin(arena_in_uW_mm),
        vmax=round(np.amax(arena_in_uW_mm))
    )

    plot_trajectory_from = i - past_trajectory
    if plot_trajectory_from < 0:
        plot_trajectory_from = 0

    plot_past_centroid, = ax_overview.plot(
        data_csv['X-Centroid'][plot_trajectory_from:i],
        data_csv['Y-Centroid'][plot_trajectory_from:i],
        c='g',
        alpha=0.3)
    '''
    plot_past_head, = ax_overview.plot(
        data_csv['X-Head'][plot_trajectory_from:i],
        data_csv['Y-Head'][plot_trajectory_from:i],
        c='c',
        alpha=0.5)
    '''
    # Add a scalebar:
    fontprops = fm.FontProperties(size=18)
    scalebar = AnchoredSizeBar(ax_overview.transData,
                               20 * pixel_per_mm, '20mm',
                               'lower right',
                               pad=0.1,
                               color='black',
                               frameon=False,
                               size_vertical=1,
                               fontproperties=fontprops)
    ax_overview.add_artist(scalebar)

    # Add the legend!
    patchList = []
    for key in legend_dict:
        data_key = mpatches.Patch(color=legend_dict[key], label=key)
        ax_overview.set_xlabel(key, fontsize=0)
        patchList.append(data_key)
    ax_overview.legend(handles=patchList, fontsize=15, framealpha=0.5,
                       loc=3)

    ax_overview.axis('off')

    time_indicator.set_xdata([i, i])
    ax_stim.set_xlim(i - 30 * 5, i + 30 * 5)

    '''
    if len(cbar_bool) == 0:
        cbar_bool.append('Done')

        cbar = fig.colorbar(plot_background,
                            cax=cax,
                            ticks=[0,
                                   1])

        cbar.set_label('625nm centered [' + r'$\mu$' + r'W/$mm^2$]',
                       fontsize=20)

        cbar.ax.tick_params(labelsize=15)
        cbar.ax.get_yaxis().labelpad = -5
    '''
    return ([plot_animal,
             plot_background,
             plot_past_centroid,
             #plot_past_head
             ])

ylim_max = 5
print('plotting now')
gs = gridspec.GridSpec(4, 4)

fig = plt.figure(figsize=(10, 10))
ax_overview = plt.subplot(gs[0:3, 0:4])
ax_stim = plt.subplot(gs[3:4, 0:4])

# The title
fig.suptitle('Adult fruit fly'
             '\n' + r"$\it{Gr66a}$" + '>' + r"$\it{CsChrimson}$" +
             '\nin a virtual taste gradient',
             fontsize=20)

# get the colorbar exactly the same size as the resulting window!
#divider = make_axes_locatable(ax_overview)
#cax = divider.append_axes('right', size='5%', pad=0.05)

arena_in_uW_mm, stimulation_file = prepare_array(0)
# use masked arrays to mask the image background where the animal is and mask the parts where the animal is not on the
# animal axis
animal_masked = np.ma.masked_array(
    stimulation_file[:, :, 2])  # ,stimulation_file[:,:,2]>0)
arena_masked = np.ma.masked_array(arena_in_uW_mm,
                                  stimulation_file[:, :, 2] > 0)

plot_animal = ax_overview.imshow(animal_masked,
                                 animated=True,
                                 interpolation='nearest',
                                 cmap='Blues')

plot_background = ax_overview.imshow(
    arena_masked,
    animated=True,
    interpolation='nearest',
    cmap=own_red_cmap,
    vmin=np.amin(arena_in_uW_mm),
    vmax=round(np.amax(arena_in_uW_mm)))

plot_past_centroid, = ax_overview.plot(data_csv['X-Centroid'][0:0],
                                       data_csv['Y-Centroid'][0:0],
                                       c='g',
                                       alpha=0.3,
                                       animated=True
                                       )

plot_past_head = ax_overview.plot(data_csv['X-Head'][0:0],
                                  data_csv['Y-Head'][0:0],
                                  c='c',
                                  alpha=0.5,
                                  animated=True)

# show a custom legend
legend_dict = { '0 ' + r'$\mu$' + r'W/$mm^2$' : own_red_cmap(np.linspace(0,1,2))[0],
               '4.5 ' + r'$\mu$' + r'W/$mm^2$' : own_red_cmap(np.linspace(0,1,2))[-1]}

###################################################
# Stim plot at bottom
###################################################
# Needed for plotting
def frame_to_time(x, pos):
    """
    The two args are the value and tick position
    The first string defines what is shown. %1.1f means that only the first decimal is shown
    x/fps means that for each 'tick', meaning each datapoint the value that is shown is divided by the frames per second.
    This way the actual seconds are displayed
    """
    # CAREFUL!!! was '%1.1f', the 0 makes sure only the value before the decimal dot is shown!!!
    # Make sure your ticks are really integers!
    return '%1.0f' % (x / fps)  #


# set x axis limit - make sure to have a multiple of the framerate to
# make sure the ticks are placed in proper distance!
start_x_tick, end_x_tick = 0, fps * 300
ax_stim.set_xlim(start_x_tick, end_x_tick)
# get one tick for every 30 seconds
stepsize = int(fps)
# set the xticks
ax_stim.xaxis.set_ticks(
    np.arange(start_x_tick, end_x_tick + fps, stepsize))
x_formatter = plticker.FuncFormatter(frame_to_time)
# and then giving this function to the major_formatter
ax_stim.xaxis.set_major_formatter(x_formatter)

plot_stimulation = ax_stim.plot(stim_in_uW_mm, color='r')

ax_stim.set_xlim(-30, 30)
time_indicator, = ax_stim.plot([0, 0],
                               [0, ylim_max],
                               color='c',
                               lw=10,
                               alpha=0.8,
                               zorder=0,
                               linestyle='-')

ax_stim.set_xlabel('Time[s]', fontsize=20)
ax_stim.set_ylabel('625nm [' + r'$\mu$' + r'W/$mm^2$]',
                   fontsize=20)
# can change x ticklabel size here
ax_stim.tick_params(axis='x', labelsize=15)
# can change y ticklable size here
ax_stim.tick_params(axis='y', labelsize=15)
ax_stim.grid(alpha=0.5)

if no_of_frames is False:
    no_of_frames = range(1, raw_images.shape[2] - 5)

# anim = animation.FuncAnimation(fig,updatefig,frames = range(raw_images.shape[2]), interval=30, blit=True)
anim = animation.FuncAnimation(fig,
                               updatefig,
                               frames=no_of_frames,
                               interval=1 / fps,
                               blit=True)
# plt.show()

# writer = animation.writers['ffmpeg'](fps=fps,codec='mpeg4',bitrate=1e6)

writer = animation.writers['ffmpeg'](fps=fps, codec='mpeg4',
                                     bitrate=1e6)

target_file_path = Path(target_path, video_name)
anim.save(str(target_file_path), writer=writer, dpi=200)
# anim.save('C:/Users/David Tadres/Desktop/video.mp4', writer=writer,
#          dpi=200)

print('done with video')

