"""
D. melanogaster larva with genotype Or42a>Chrimson in a volcano
shaped gradient
"""

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from matplotlib import animation
import matplotlib.gridspec as gridspec
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pathlib import Path
import json
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
import matplotlib.font_manager as fm

fps = 60
video_name = 'Movie_4_new.mp4'
# How long in the past should the head and centroid position be plotted?
past_trajectory = 20  # in seconds

# FOR TESTING
no_of_frames = False  # Set to False to get full video! (
# Well, almost full, the last 5 frames (of 9000) are not taken into
# account)

# colormap = 'magma'
# viridis
# plasma
# inferno
# magma

# Organize the path - everything's relative to the location of the
# script being used!
home_path = os.getcwd()
target_path = Path(home_path + '/Created_Movies/')
os.makedirs(target_path, exist_ok=True)
data_path = Path(home_path +
                 '/data/21.02.2018_17-57-17_MS379xMS133/')
os.chdir(data_path)

# Read the binary images...
raw_images = np.load('sm_thresh.npy')  # todo rename!
# ...and the coordinates the each image is located...
bounding_boxes = np.load('bounding_boxes.npy')
# ...and the delivered stimulation
stimulation = np.load('stimulation.npy')
# and finally the stimulation file for the background
stimulation_file_original = \
    np.genfromtxt(
        '640x480_vulcano_animal_pos[320,240,0.0].csv',
        delimiter=',')
# Need to read the data.csv file as well
path_object = Path(data_path).glob('**/*')
files = [x for x in path_object if x.is_file()]
for i in files:
    if 'data.csv' in str(i):
        data_csv = pd.read_csv(i, delimiter=',')

with open(('experiment_settings.json'), 'r') as file:
    experiment_settings = json.load(file)
    pixel_per_mm = experiment_settings['Pixel per mm']
    recording_time = experiment_settings['Recording time']
    recording_fps = experiment_settings['Framerate']

past_trajectory *= recording_fps

# create first image, make it a 3 channel image
stimulation_file = np.zeros((stimulation_file_original.shape[0],
                             stimulation_file_original.shape[1],
                             3))

# get the virtual arena in uW/mm2
measured_light_intensity = 2

# get stimulation in uW/mm2
stim_in_uW_mm = stimulation * (measured_light_intensity / 40000)

cbar_bool = []


def prepare_array(i):
    """
    Prepare the array for plotting
    """
    stimulation_file = np.zeros((stimulation_file_original.shape[0],
                                 stimulation_file_original.shape[1],
                                 3))

    # Valid floating point input is 0..1 - normalize
    stimulation_file[:, :, 0] = stimulation_file_original.copy()

    row_min = bounding_boxes[0, i]
    row_max = bounding_boxes[1, i]
    col_min = bounding_boxes[2, i]
    col_max = bounding_boxes[3, i]

    area_of_animal = stimulation_file[row_min:row_max, col_min:col_max,
                     :]

    # turn the blue channel ON
    area_of_animal[
        raw_images[0:int(bounding_boxes[1, i] - bounding_boxes[0, i]),
        0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
        i], 2] = 1

    # and the others off to only have blue left
    area_of_animal[
        raw_images[0:int(bounding_boxes[1, i] - bounding_boxes[0, i]),
        0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
        i], 0] = 1

    area_of_animal[
        raw_images[0:int(bounding_boxes[1, i] - bounding_boxes[0, i]),
        0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
        i], 1] = 1

    # max value in stim file is 40000
    arena_in_uW_mm = stimulation_file[:, :, 0] \
                     * (measured_light_intensity / 40000)
    return (arena_in_uW_mm, stimulation_file)


def updatefig(i):
    print(i)

    ax_overview.cla()

    arena_in_uW_mm, stimulation_file = prepare_array(i)

    # use masked arrays to mask the image background where the animal
    # is and mask the parts where the animal is not on the animal axis
    animal_masked = np.ma.masked_array(
        stimulation_file[:, :, 2])  # ,stimulation_file[:,:,2]>0)
    arena_masked = np.ma.masked_array(arena_in_uW_mm,
                                      stimulation_file[:, :, 2] > 0)

    plot_animal = ax_overview.imshow(animal_masked,
                                     animated=True,
                                     interpolation='nearest',
                                     cmap='Blues')

    plot_background = ax_overview.imshow(
        arena_masked,
        animated=True,
        interpolation='nearest',
        cmap='Reds',
        vmin=np.amin(arena_in_uW_mm),
        vmax=round(np.amax(arena_in_uW_mm))
    )

    plot_trajectory_from = i - past_trajectory
    if plot_trajectory_from < 0:
        plot_trajectory_from = 0

    plot_past_centroid, = ax_overview.plot(
        data_csv['X-Centroid'][plot_trajectory_from:i],
        data_csv['Y-Centroid'][plot_trajectory_from:i],
        c='g',
        alpha=0.3,
        label = 'Centroid')

    plot_past_head, = ax_overview.plot(
        data_csv['X-Head'][plot_trajectory_from:i],
        data_csv['Y-Head'][plot_trajectory_from:i],
        c='c',
        alpha=0.5,
        label = 'Head')

    # add legend again
    ax_overview.legend(loc='upper left', prop={'size': 15})

    # Add a scalebar:
    fontprops = fm.FontProperties(size=18)
    scalebar = AnchoredSizeBar(ax_overview.transData,
                               20 * pixel_per_mm, '20mm',
                               'lower right',
                               pad=0.1,
                               color='black',
                               frameon=False,
                               size_vertical=1,
                               fontproperties=fontprops)
    ax_overview.add_artist(scalebar)

    ax_overview.axis('off')

    current_x_position = i/recording_fps
    time_indicator.set_xdata([current_x_position, current_x_position])
    ax_stim.set_xlim(current_x_position - 3, current_x_position + 3)

    if len(cbar_bool) == 0:
        cbar_bool.append('Done')

        cbar = fig.colorbar(plot_background,
                            cax=cax,
                            ticks=[0,
                                   1])

        cbar.set_label('625nm centered [' + r'$\mu$' + r'W/$mm^2$]',
                       fontsize=20)

        cbar.ax.tick_params(labelsize=15)
        cbar.ax.get_yaxis().labelpad = -5

    return ([plot_animal,
             plot_background,
             plot_past_centroid,
             plot_past_head
             ])


ylim_max = 1
print('plotting now')
gs = gridspec.GridSpec(4, 4)

fig = plt.figure(figsize=(10, 10))
ax_overview = plt.subplot(gs[0:3, 0:4])
ax_stim = plt.subplot(gs[3:4, 0:4])

# The title
# The title
fig.suptitle('Fruit fly larva'
             '\n' + r"$\it{Or42a}$" + '>'
             + r"$\it{CsChrimson,Orco^{"r"-/-}}$"
             '\nin a virtual odor gradient',
             fontsize=20)

# get the colorbar exactly the same size as the resulting window!
divider = make_axes_locatable(ax_overview)
cax = divider.append_axes('right', size='5%', pad=0.05)

arena_in_uW_mm, stimulation_file = prepare_array(0)
# use masked arrays to mask the image background where the animal is and mask the parts where the animal is not on the
# animal axis
animal_masked = np.ma.masked_array(
    stimulation_file[:, :, 2])  # ,stimulation_file[:,:,2]>0)
arena_masked = np.ma.masked_array(arena_in_uW_mm,
                                  stimulation_file[:, :, 2] > 0)

plot_animal = ax_overview.imshow(animal_masked,
                                 animated=True,
                                 interpolation='nearest',
                                 cmap='Blues')

plot_background = ax_overview.imshow(
    arena_masked,
    animated=True,
    interpolation='nearest',
    cmap='Reds',
    vmin=np.amin(arena_in_uW_mm),
    vmax=round(np.amax(arena_in_uW_mm)))

plot_past_centroid, = ax_overview.plot(data_csv['X-Centroid'][0:0],
                                       data_csv['Y-Centroid'][0:0],
                                       c='g',
                                       alpha=0.3,
                                       animated=True,
                                       label='Centroid'
                                       )

plot_past_head = ax_overview.plot(data_csv['X-Head'][0:0],
                                  data_csv['Y-Head'][0:0],
                                  c='c',
                                  alpha=0.5,
                                  animated=True,
                                  label='Head'
                                  )

# Get a legend
ax_overview.legend(loc='upper left', prop={'size': 15})

###################################################
# Stim plot at bottom
###################################################
# Needed for plotting
def frame_to_time(x, pos):
    """
    The two args are the value and tick position
    The first string defines what is shown. %1.1f means that only the first decimal is shown
    x/fps means that for each 'tick', meaning each datapoint the value that is shown is divided by the frames per second.
    This way the actual seconds are displayed
    """
    # CAREFUL!!! was '%1.1f', the 0 makes sure only the value before the decimal dot is shown!!!
    # Make sure your ticks are really integers!
    return '%1.0f' % (x / fps)  #


# set x axis limit - make sure to have a multiple of the framerate to
# make sure the ticks are placed in proper distance!
#start_x_tick, end_x_tick = 0, fps * 300
#ax_stim.set_xlim(start_x_tick, end_x_tick)
# get one tick for every 30 seconds
#stepsize = int(fps)
# set the xticks
#ax_stim.xaxis.set_ticks(
#    np.arange(start_x_tick, end_x_tick + fps, stepsize))
#x_formatter = plticker.FuncFormatter(frame_to_time)
# and then giving this function to the major_formatter
#ax_stim.xaxis.set_major_formatter(x_formatter)

x_values = np.arange(0,stim_in_uW_mm.shape[0]/recording_fps,
                     1/recording_fps)
plot_stimulation = ax_stim.plot(x_values,
                                stim_in_uW_mm, color='r')

ax_stim.set_xlim(-3, 3)
time_indicator, = ax_stim.plot([0, 0],
                               [0, ylim_max],
                               color='c',
                               lw=10,
                               alpha=0.8,
                               zorder=0,
                               linestyle='-')

ax_stim.set_xlabel('Time[s]', fontsize=20)
ax_stim.set_ylabel('625nm [' + r'$\mu$' + r'W/$mm^2$]',
                   fontsize=20)
# can change x ticklabel size here
ax_stim.tick_params(axis='x', labelsize=15)
# can change y ticklable size here
ax_stim.tick_params(axis='y', labelsize=15)
ax_stim.grid(alpha=0.5)

if no_of_frames is False:
    no_of_frames = range(1, raw_images.shape[2] - 5)

# anim = animation.FuncAnimation(fig,updatefig,frames = range(raw_images.shape[2]), interval=30, blit=True)
anim = animation.FuncAnimation(fig,
                               updatefig,
                               frames=no_of_frames,
                               interval=1 / fps,
                               blit=True)
# plt.show()

# writer = animation.writers['ffmpeg'](fps=fps,codec='mpeg4',bitrate=1e6)

writer = animation.writers['ffmpeg'](fps=fps, codec='mpeg4',
                                     bitrate=1e6)

target_file_path = Path(target_path, video_name)
anim.save(str(target_file_path), writer=writer, dpi=200)
# anim.save('C:/Users/David Tadres/Desktop/video.mp4', writer=writer,
#          dpi=200)

print('done with video')

