"""
Some ideas of how to improve the video I've taken:
Have more than one video - e.g. on the left the real video,
in the middle the animal in the virtual reality and on the right
the temporal change of light stimulation.

We're lucky that the recording fps of the handheld video recorder is
59.94fps and therefore almost double the framerate of PiVR.

While there's a slight discrepancy I do not think it's really noticeable
(1/30 = 0.033333, 1/60 = 0.01666 and 1/59.94 = 0.016683....) The
error per frame is therefore in the order of 0.00002seconds. We have
a total of 1800frames so the drift should be
((1/59.94) - (1/60))*1800 ~=0.030 which is only about one frame for
the whole video.

Also, used a Canon camcorder that uses the AHVC video file. ffmpeg is 
not able to read that file format properly.
To convert, I downloaded the libav 
(http://builds.libav.org/windows/nightly-gpl/), copied the file to 
be converted into the bin folder and type:
avconv -i 00174.MTS -c:a copy -c:v copy 00174.mp4
"""

import imageio
import matplotlib.pyplot as plt
from matplotlib import animation
from pathlib import Path
import numpy as np
import json
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
import matplotlib.font_manager as fm
import os

video_name = 'Movie_1.mp4'
fps = 30 # how fast the video that is being created here is
first_frame_real_video = 72 # determined empirically by checking at
#no_of_frames = 1800 # only first minute is interesting!
no_of_frames = 1800
# which frame the preview on PiVR got enlarged (=animal detection
# finished)
measured_light_intensity = 2 # This is also an empirical number

# Organize the path - everything's relative to the location of the
# script being used!
home_path = os.getcwd()
target_path = Path(home_path + '/Created_Movies/')
#target_path = Path('G:\My Drive\PhD\Papers\RasPi larval
# tracker\InitialDraft\Movies\Movie 1')
os.makedirs(target_path, exist_ok=True)
# First, it is necessary to load the recorded movie. Actually,
# the movie was in a different format before (MTS) which could not be
# read by imageio. I converted it using avconv to this mp4.
movie_path = Path(home_path +
                 '/data/')
#os.chdir(movie_path)
# Get the video object and read the metadata
reader = imageio.get_reader(str(movie_path) + '\\00013.mp4')
fps_video = reader.get_meta_data()['fps']
total_frames = reader.get_meta_data()['nframes']
framesize = reader.get_meta_data()['size']
duration = reader.get_meta_data()['duration']

# Now, read the data of PiVR recorded while the video above was
# recorded
data_path = Path(home_path +
                 '/data/2019.11.15_15-35-57_checkerboard_adult/')
# Get the virtual arena presented
vr_arena = np.genfromtxt(str(data_path) + "\\640x480_checkerboard.csv",
                         delimiter=',')
heads = np.load(str(data_path) + '\\heads.npy')
stimulation = np.load(str(data_path) + '\\stimulation.npy')
# Read the binary images...
binary_images = np.load(str(data_path) + '\\sm_thresh.npy')
# ...and the coordinates the each image is located...
bounding_boxes = np.load(str(data_path) + '\\bounding_boxes.npy')

with open((str(data_path) + '\\experiment_settings.json'), 'r') as file:
    experiment_settings = json.load(file)
    pixel_per_mm = experiment_settings['Pixel per mm']
    recording_time_PiVR = experiment_settings['Recording time']
    fps_PiVR = experiment_settings['Framerate']

##################
# FUNCTIONS
##################

def updatefig(i):
    """
    Takes the plotted axis objects, clears them and plots the next frame
    """
    print(i)

    # Leftmost axis
    ax_real.cla()
    # 413 is the first frame in the video where the experiment starts
    # the framerate of the video is ~double the speed of PiVR,
    # therefore only every 2nd video frame is taken to be plotted
    video_plot = ax_real.imshow(reader.get_data(
         first_frame_real_video+int(round(
                            i*2)))[:,250:1800,:])
    ax_real.axis('off')

    # Middle Axis
    ax_vr.cla()
    arena_in_uW_mm, stimulation_file = prepare_array(i)

    # use masked arrays to mask the image background where the animal
    # is and mask the parts where the animal is not on the animal axis
    animal_masked = np.ma.masked_array(
          stimulation_file[:, :, 2])  # ,stimulation_file[:,:,2]>0)
    arena_masked = np.ma.masked_array(arena_in_uW_mm,
                                       stimulation_file[:, :, 2] > 0)

    plot_animal = ax_vr.imshow(animal_masked,
                                      animated=True,
                                      interpolation='nearest',
                                      cmap='Blues')

    plot_background = ax_vr.imshow(
          arena_masked,
          animated=True,
          interpolation='nearest',
          cmap='Reds',
          vmin=np.amin(arena_in_uW_mm),
          vmax=round(np.amax(arena_in_uW_mm)),
          alpha=0.5
     )
    # Add a scalebar:
    fontprops = fm.FontProperties(size=18)
    scalebar = AnchoredSizeBar(ax_vr.transData,
                               20 * pixel_per_mm, '20mm',
                               'lower right',
                               pad=0.1,
                               color='black',
                               frameon=False,
                               size_vertical=1,
                               fontproperties=fontprops)
    ax_vr.add_artist(scalebar)

    ax_vr.axis('off')

    current_x_position = i / fps_PiVR
    time_indicator.set_xdata([current_x_position, current_x_position])
    ax_stim.set_xlim(current_x_position - 3, current_x_position + 3)

    return ([video_plot,
             plot_animal,
             plot_background
             ])

def prepare_array(i):
    """
    Prepare the array for plotting
    """
    stimulation_file = np.zeros((vr_arena.shape[0],
                                 vr_arena.shape[1],
                                 3))

    # Valid floating point input is 0..1 - normalize
    stimulation_file[:, :, 0] = vr_arena.copy()

    row_min = bounding_boxes[0, i]
    row_max = bounding_boxes[1, i]
    col_min = bounding_boxes[2, i]
    col_max = bounding_boxes[3, i]

    area_of_animal = stimulation_file[row_min:row_max, col_min:col_max,
                     :]

    # turn the blue channel ON
    area_of_animal[
     binary_images[0:int(bounding_boxes[1, i] - bounding_boxes[0, i]),
        0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
        i], 2] = 1

    # and the others off to only have blue left
    area_of_animal[
        binary_images[0:int(bounding_boxes[1, i] - bounding_boxes[0,i]),
        0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
        i], 0] = 1

    area_of_animal[
        binary_images[0:int(bounding_boxes[1, i] - bounding_boxes[0,i]),
        0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
        i], 1] = 1

    # max value in stim file is 40000
    arena_in_uW_mm = stimulation_file[:, :, 0] \
                     * (measured_light_intensity / 100)
    return(arena_in_uW_mm, stimulation_file)

#################
# PLOTTING
#################
# video starts at index 413
# The video framerate is ~double the framerate of PiVR. This might be
# easy: just skip every other frame of the video!

fig = plt.figure(figsize=(15,5))
# plot the left axis
l = 0.05
b = 0.05
w = 0.30
h = 0.9
rect = l,b,w,h
ax_real = fig.add_axes(rect)
video_plot = ax_real.imshow(reader.get_data(
    first_frame_real_video)[:,250:1800,:])
ax_real.axis('off')

arena_in_uW_mm, stimulation_file = prepare_array(0)
animal_masked = np.ma.masked_array(stimulation_file[:,:,2])#,stimulation_file[:,:,2]>0)
arena_masked = np.ma.masked_array(arena_in_uW_mm,stimulation_file[:,:,2]>0)

######
# plot the middle axis
# Had to manually place the axis, gridspec left too much white space
# on the left and right side of the video!
######
l = 0.36
b = 0.05
w = 0.30
h = 0.9
rect = l,b,w,h
ax_vr = fig.add_axes(rect)
ax_vr.imshow(animal_masked,
             animated=True,
             interpolation='nearest',
             cmap='Blues')

ax_vr.imshow(arena_masked,
             animated = True,
             interpolation='nearest',
             cmap='Reds',
             vmin=np.amin(arena_in_uW_mm),
             vmax=round(np.amax(arena_in_uW_mm)),
             alpha=0.8)

ax_vr.axis('off')

######
# plot the right axis
l = 0.72
b = 0.15
w = 0.25
h = 0.75
rect = l,b,w,h
ax_stim = fig.add_axes(rect)
x = np.arange(0,recording_time_PiVR, 1/fps_PiVR)
ax_stim.plot(x, stimulation*(measured_light_intensity/100))
ax_stim.tick_params(axis='x', labelsize=15)
ax_stim.tick_params(axis='y', labelsize=15)
ax_stim.set_yticks(np.arange(0,3,1))
ax_stim.set_xlabel('Time[s]', fontsize=20)
ax_stim.set_ylabel('625nm [' + r'$\mu$' + r'W/$mm^2$]', fontsize=20)
ax_stim.set_xlim(-3,3)

ylim_max = 2.1 #uW/mm2
time_indicator, = ax_stim.plot([0,0],
                               [0,ylim_max],
                               color='c',
                               lw=10,
                               alpha=0.8,
                               zorder=0,
                               linestyle='-')

anim = animation.FuncAnimation(fig,
                               updatefig,
                               frames = no_of_frames,
                               interval=1/fps_PiVR,
                               blit=True)
writer = animation.writers['ffmpeg'](fps=fps,codec='mpeg4',bitrate=1e6)
target_file_path = Path(target_path, video_name)
anim.save(str(target_file_path), writer=writer,dpi=300)

print('done with video')
