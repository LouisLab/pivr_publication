"""
D. melanogaster larva with genotype Or42a>Orco;Orco-/- in a IAA gradient

"""

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from matplotlib import animation
import matplotlib.gridspec as gridspec
import matplotlib.ticker as plticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from pathlib import Path
import json
import imageio
from PIL import Image
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
import matplotlib.font_manager as fm


fps = 60
video_name = 'Movie_2_new.mp4'
start_simulation_at_time = 30 # in seconds
# How long in the past should the head and centroid position be plotted?
past_trajectory = 20 # in seconds

# FOR TESTING
no_of_frames = False  # Set to False to get full video! (
# Well, almost
# full, the last 5 frames (of 9000) are not taken into account)

# colormap = 'magma'
# viridis
# plasma
# inferno
# magma

# Organize the path - everything's relative to the location of the
# script being used!
home_path = os.getcwd()
target_path = Path(home_path + '/Created_Movies/')
os.makedirs(target_path, exist_ok=True)
data_path = Path(home_path +
                 '/data/2019.11.18_17-26-47_MS74xMS133/')
os.chdir(data_path)

# Read the binary images...
raw_images = np.load('sm_thresh.npy')
# ...and the coordinates the each image is located...
bounding_boxes = np.load('bounding_boxes.npy')

try:
    stimulation_file_original = \
        np.load(str(data_path) +
                str(Path('//normalized_diffusion_time_series.npy')))
except FileNotFoundError:
    # and finally the calculated odor concentration - this includes a
    # time dimension, of course
    stimulation_file_original = np.load('diffusion_time_series.npy')
    # Taking the data from Matthieu's 2008 paper - I have to scale the
    # maximum of the simulated odor source
    # The value "100" is the simulated concentration from that paper! It
    # indicates that the maximum value during the simulation is 100uM!
    #upscale_factor = 100/np.amax(stimulation_file_original)
    #stimulation_file_original *= upscale_factor
    # However, due to plotting the values have to be between 0 and 1!
    # This of course means that the value "1" is indicates a value of 100uM
    upscale_factor = 1/np.amax(stimulation_file_original)
    stimulation_file_original *= upscale_factor
    np.save(str(data_path) + str(Path(
        '//normalized_diffusion_time_series.npy')),
            stimulation_file_original)

max_stimulation_value = np.amax(stimulation_file_original)
# Need to read the data.csv file as well
path_object = Path(data_path).glob('**/*')
files = [x for x in path_object if x.is_file()]
for i in files:
    if 'data.csv' in str(i):
        data_csv = pd.read_csv(i, delimiter=',')

background = imageio.imread('Background.jpg')
with open(('experiment_settings.json'), 'r') as file:
    experiment_settings = json.load(file)
    pixel_per_mm = experiment_settings['Pixel per mm']
    recording_time = experiment_settings['Recording time']
    recording_fps = experiment_settings['Framerate']
    source_x = experiment_settings['Source x']
    source_y = experiment_settings['Source y']

past_trajectory *= recording_fps

# As the simulation of the odor is in cm, and our px/mm is in ... well mm, correct that
pixel_per_cm = pixel_per_mm*10

# the maximum in the calculated odor gradient is of course at 500,500
simulated_odor_max_x = 500
simulated_odor_max_y = 500

# one pixel in the calculated odor gradient is...1x10^-4m...as the diameter_vector is defined as 10cm and has 0.01 steps
# we end up with 0.1mm/px
sim_pixel_per_mm = 10

# see whether the dish is 10cm or less
ten_cm = True
if background.shape[0]/pixel_per_cm >= 10:
    # doing the int(round()) makes it imprecise of course! Should be ok as we should only be off by maximum half a pixel and
    # this is for visualization only!
    ten_cm_index_relative_to_real_source_y =\
        np.asarray((int(round(source_y - (pixel_per_cm*5))), \
                    int(round(source_y + (pixel_per_cm*5)))))
    ten_cm_index_relative_to_real_source_x = \
        np.asarray((int(round(source_x - (pixel_per_cm*5))), \
                    int(round(source_x + (pixel_per_cm*5)))))
elif background.shape[0]/pixel_per_cm >=8:
    ten_cm = False
    eight_cm_index_relative_to_real_source_y = \
        np.asarray((int(round(source_y - (pixel_per_cm*4))), \
                    int(round(source_y + (pixel_per_cm*4)))))
    eight_cm_index_relative_to_real_source_x = \
        np.asarray((int(round(source_x - (pixel_per_cm*4))), \
                    int(round(source_x + (pixel_per_cm*4)))))

cbar_bool = []

def prepare_array(i,counter_arena, place_larva=True):
    """
    Prepare the array for plotting
    """
    stimulation_file = np.zeros((background.shape[0],
                                 background.shape[1],
                                 3))

    # IMPORTANT: The simulated odor has a time resolution of 1 second
    # As the experiment was run at 30fps, the simulated odor needs to
    # be updated every 30 iterations
    if i%recording_fps == 0:
        if i != 0:
            counter_arena.append(counter_arena[-1]+1)
            print('arena counter set to ' + repr(counter_arena[-1]))


    # Get the bounding box in the experiment reference frame
    row_min = bounding_boxes[0, i]
    row_max = bounding_boxes[1, i]
    col_min = bounding_boxes[2, i]
    col_max = bounding_boxes[3, i]

    # Need to resize the arena:
    # Convert to PIL.Image object
    stimulation_file_resized = \
        Image.fromarray(
            stimulation_file_original[counter_arena[-1], :, :])
    # How large is the resized simulated arena?
    #
    # sim_pixel_per_mm # is the pixel/mm of the simulation
    # pixel_per_mm # Is the pixel/mm of the real image
    size_sim_odor = int(
        pixel_per_mm/sim_pixel_per_mm
        *stimulation_file_original.shape[1])
    stimulation_file_resized = \
        np.asarray(stimulation_file_resized.resize((
            size_sim_odor, size_sim_odor)))

    # The simulated odor source is exactly in the center!
    sim_odor_center_x = sim_odor_center_y = int(round(size_sim_odor/2))

    # Now place the simulated odor in the frame - this is possible as
    # the odor source coordinate is know for both the simulation and
    # for reality
    real_x_left = int(round(source_x - sim_odor_center_x))
    real_x_right = int(round(source_x + sim_odor_center_x))

    # REMEMBER: 0 is at the bottom, 480 at the top
    real_y_top = int(round(source_y + sim_odor_center_y))
    real_y_bottom = int(round(source_y - sim_odor_center_y))

    # do some housekeeping: The indexes shouldn't be below 0 or
    # larger than the sides:
    if real_x_left < 0:
        sim_x_left = real_x_left * -1 # E.g. if -5, start at 5
        real_x_left = 0
    else:
        sim_x_left = 0

    if real_x_right > background.shape[1]:
        sim_x_right = size_sim_odor - real_x_right
        real_x_right = background.shape[1]
    else:
        sim_x_right = size_sim_odor

    if real_y_bottom < 0:
        sim_y_bottom = real_y_bottom * -1
        real_y_bottom = 0
    else:
        sim_y_bottom = 0

    if real_y_top > background.shape[0]:
        sim_y_top = size_sim_odor - real_y_top
        real_y_top = background.shape[0]
    else:
        sim_y_top = size_sim_odor

    # IF the shapes don't agree (shouldn't be more than one) just
    # move the inner by one
    if (sim_y_top - sim_y_bottom) < (real_y_top-real_y_bottom):
        real_y_bottom += 1
    if (sim_y_top - sim_y_bottom) > (real_y_top-real_y_bottom):
        real_y_bottom -= 1

    if (sim_x_right - sim_x_left) < (real_x_right - real_x_left):
        real_x_left += 1
    if (sim_x_right - sim_x_left) < (real_x_right - real_x_left):
        real_x_left -= 1

    # Valid floating point input is 0..1 - normalize
    # Put  this in the GREEN channel
    stimulation_file[real_y_bottom:real_y_top,
                     real_x_left:real_x_right,
                    1] = \
        stimulation_file_resized[
        sim_y_bottom:sim_y_top,
        sim_x_left:sim_x_right].copy()

    if place_larva:
        area_of_animal = \
            stimulation_file[row_min:row_max, col_min:col_max,:]

        # turn the blue channel ON
        area_of_animal[
            raw_images[0:int(bounding_boxes[1, i] - bounding_boxes[0, i]),
            0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
            i], 2] = 1

        # and the others off to only have blue left
        area_of_animal[
            raw_images[0:int(bounding_boxes[1, i] - bounding_boxes[0, i]),
            0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
            i], 0] = 0

        area_of_animal[
            raw_images[0:int(bounding_boxes[1, i] - bounding_boxes[0, i]),
            0:int(bounding_boxes[3, i] - bounding_boxes[2, i]),
            i], 1] = 0
    # max value in stim file is 100
    arena_in_uM = stimulation_file[:, :, 1] \
                     * (100 / max_stimulation_value)
    return (arena_in_uM, stimulation_file)

def updatefig(i):
    print(i)

    ax_overview.cla()

    arena_in_uM, stimulation_file = prepare_array(i,counter_arena)

    # use masked arrays to mask the image background where the animal
    # is and mask the parts where the animal is not on the animal axis
    animal_masked = np.ma.masked_array(
        stimulation_file[:, :, 2])  # ,stimulation_file[:,:,2]>0)
    arena_masked = np.ma.masked_array(arena_in_uM,
                                      stimulation_file[:, :, 2] > 0)

    plot_animal = ax_overview.imshow(
        animal_masked,
        animated=True,
        interpolation='nearest',
        cmap='Blues')

    plot_background = ax_overview.imshow(
        arena_masked,
        animated=True,
        interpolation='nearest',
        cmap='Greens',
        vmin=0,
        vmax=100
        )

    if past_trajectory != 0:
        plot_trajectory_from = i - past_trajectory
        if plot_trajectory_from < 0:
            plot_trajectory_from = 0


        plot_past_centroid, = ax_overview.plot(
            data_csv['X-Centroid'][plot_trajectory_from:i],
            data_csv['Y-Centroid'][plot_trajectory_from:i],
            c='r',
            alpha=0.3,
            label='Centroid')

        plot_past_head, = ax_overview.plot(
            data_csv['X-Head'][plot_trajectory_from:i],
            data_csv['Y-Head'][plot_trajectory_from:i],
            c='c',
            alpha=0.5,
            label='Head')

    # add legend again
    ax_overview.legend(loc='upper left', prop={'size': 15})

    # Add a scalebar:
    fontprops = fm.FontProperties(size=18)
    scalebar = AnchoredSizeBar(ax_overview.transData,
                               20 * pixel_per_mm, '20mm',
                               'lower right',
                               pad=0.1,
                               color='black',
                               frameon=False,
                               size_vertical=1,
                               fontproperties=fontprops)
    ax_overview.add_artist(scalebar)

    ax_overview.axis('off')

    current_x_position = i/recording_fps
    time_indicator.set_xdata([current_x_position, current_x_position])
    ax_stim.set_xlim(current_x_position - 3, current_x_position + 3)

    if len(cbar_bool) == 0:
        cbar_bool.append('Done')

        cbar = fig.colorbar(plot_background,
                            cax=cax,
                            ticks=[0,100])

        cbar.set_label(#'625nm centered [' + r'$\mu$' + r'W/$mm^2$]',
                       'IAA concentration [uM]',
                       fontsize=20)

        cbar.ax.tick_params(labelsize=15)
        cbar.ax.get_yaxis().labelpad = -5

    return ([plot_animal,
             plot_background,
             plot_past_centroid,
             plot_past_head
             ])


# Before starting to plot, we need to go through the experiment once
# to extract all the experienced (simulated!) sensory experience!
try:
    all_sensory_experience = \
        np.load(str(data_path) + str(Path(
            '//experienced_stimulation.npy')))
except FileNotFoundError:
    counter_arena = [start_simulation_at_time]
    all_sensory_experience = np.zeros((data_csv.shape[0]))
    print('Calculating experienced Stimulation...')
    for i in range(data_csv.shape[0]):
        if i % 100 == 0:
            print(repr(i) + ' of ' + repr(data_csv.shape[0]))
        arena_in_uM, stimulation_file = prepare_array(i,
                                                      counter_arena,
                                                      place_larva=False)
        try:
            all_sensory_experience[i] = \
                arena_in_uM[int(data_csv['Y-Head'][i]),
                            int(data_csv['X-Head'][i])
                            ]
        except ValueError:
            # Happens if there's a NaN, just keep it at zero
            pass

    np.save(str(data_path) + str(Path(
        '//experienced_stimulation.npy')),
            all_sensory_experience)

counter_arena = [start_simulation_at_time]
ylim_max = 100
print('plotting now')
gs = gridspec.GridSpec(4, 4)

fig = plt.figure(figsize=(10, 10))
ax_overview = plt.subplot(gs[0:3, 0:4])
ax_stim = plt.subplot(gs[3:4, 0:4])

# The title
fig.suptitle('Fruit fly larva'
             '\n' + r"$\it{Or42a}$" + '>'
             + r"$\it{Orco,Orco^{"r"-/-}}$" +
             '\nin an isoamyl-acetate gradient',
             fontsize=20)

# get the colorbar exactly the same size as the resulting window!
divider = make_axes_locatable(ax_overview)
cax = divider.append_axes('right', size='5%', pad=0.05)

arena_in_uM, stimulation_file = prepare_array(0, counter_arena)
# use masked arrays to mask the image background where the animal is and mask the parts where the animal is not on the
# animal axis
animal_masked = np.ma.masked_array(
    stimulation_file[:, :, 2])  # ,stimulation_file[:,:,2]>0)
arena_masked = np.ma.masked_array(arena_in_uM,
                                  stimulation_file[:, :, 2] > 0)

plot_animal = ax_overview.imshow(animal_masked,
                                 animated=True,
                                 interpolation='nearest',
                                 cmap='Blues')

plot_background = ax_overview.imshow(
    arena_masked,
    animated=True,
    interpolation='nearest',
    cmap='Greens',
    vmin=0,
    vmax=100)

plot_past_centroid, = ax_overview.plot(data_csv['X-Centroid'][0:0],
                                       data_csv['Y-Centroid'][0:0],
                                       c='g',
                                       alpha=0.3,
                                       animated=True,
                                       label='Centroid'
                                       )

plot_past_head = ax_overview.plot(data_csv['X-Head'][0:0],
                                  data_csv['Y-Head'][0:0],
                                  c='c',
                                  alpha=0.5,
                                  animated=True,
                                  label='Head')

# Get a legend
ax_overview.legend(loc='upper left', prop={'size': 15})

###################################################
# Stim plot at bottom
###################################################
# Needed for plotting
def frame_to_time(x, pos):
    """
    The two args are the value and tick position
    The first string defines what is shown. %1.1f means that only the first decimal is shown
    x/fps means that for each 'tick', meaning each datapoint the value that is shown is divided by the frames per second.
    This way the actual seconds are displayed
    """
    # CAREFUL!!! was '%1.1f', the 0 makes sure only the value before the decimal dot is shown!!!
    # Make sure your ticks are really integers!
    return '%1.0f' % (x / fps)  #


# set x axis limit - make sure to have a multiple of the framerate to
# make sure the ticks are placed in proper distance!
#start_x_tick, end_x_tick = 0, fps * 300
#ax_stim.set_xlim(start_x_tick, end_x_tick)
# get one tick for every 30 seconds
#stepsize = int(fps)
# set the xticks
#ax_stim.xaxis.set_ticks(
#    np.arange(start_x_tick, end_x_tick + fps, stepsize))
#x_formatter = plticker.FuncFormatter(frame_to_time)
# and then giving this function to the major_formatter
#ax_stim.xaxis.set_major_formatter(x_formatter)

x_values = np.arange(0,all_sensory_experience.shape[0]/recording_fps,
                     1/recording_fps)
plot_stimulation = ax_stim.plot(x_values,
                                all_sensory_experience,
                                color='g')

ax_stim.set_xlim(-3, 3)
time_indicator, = ax_stim.plot([0, 0],
                               [0, ylim_max],
                               color='c',
                               lw=10,
                               alpha=0.8,
                               zorder=0,
                               linestyle='-')

ax_stim.set_xlabel('Time[s]', fontsize=20)
ax_stim.set_ylabel('IAA concentration \n[uM]',
                   fontsize=20)
# can change x ticklabel size here
ax_stim.tick_params(axis='x', labelsize=15)
# can change y ticklable size here
ax_stim.tick_params(axis='y', labelsize=15)
ax_stim.grid(alpha=0.5)

if no_of_frames is False:
    no_of_frames = range(1, raw_images.shape[2] - 5)

# anim = animation.FuncAnimation(fig,updatefig,frames = range(raw_images.shape[2]), interval=30, blit=True)
anim = animation.FuncAnimation(fig,
                               updatefig,
                               frames=no_of_frames,
                               interval=1 / fps,
                               blit=True)
# plt.show()

# writer = animation.writers['ffmpeg'](fps=fps,codec='mpeg4',bitrate=1e6)

writer = animation.writers['ffmpeg'](fps=fps, codec='mpeg4',
                                     bitrate=1e6)

target_file_path = Path(target_path, video_name)
anim.save(str(target_file_path), writer=writer, dpi=200)
# anim.save('C:/Users/David Tadres/Desktop/video.mp4', writer=writer,
#          dpi=200)

print('done with video')

